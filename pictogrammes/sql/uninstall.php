<?php
// security : if the Prestashop constant (version number) does not exists => stops the module from loading
if (!defined('_PS_VERSION_')) {
    exit;
}

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_group_lang`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_group_shop`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_lang`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_shop`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_group`;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}