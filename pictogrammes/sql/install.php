<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

$sql = array(
    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_group`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogramme_group` (
        `id_pictogramme_group` INT(11) NOT NULL AUTO_INCREMENT,
        `position` INT(11) NOT NULL,
        PRIMARY KEY (`id_pictogramme_group`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_group_lang`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogramme_group_lang` (
        `id_pictogramme_group` INT(11) NOT NULL,
        `id_lang` INT(11) NOT NULL,
        `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
        PRIMARY KEY (`id_pictogramme_group`,`id_lang`),
        KEY (`id_pictogramme_group`),
        KEY (`id_lang`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_group_shop`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogramme_group_shop` (
        `id_pictogramme_group` INT(11) NOT NULL,
        `id_shop` INT(11) NOT NULL,
        PRIMARY KEY (`id_pictogramme_group`,`id_shop`),
        KEY (`id_pictogramme_group`),
        KEY (`id_shop`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogramme` (
        `id_pictogramme` INT(11) NOT NULL AUTO_INCREMENT,
        `id_pictogramme_group` INT(11) NOT NULL,
        `position` INT(11) NOT NULL,
        PRIMARY KEY (`id_pictogramme`),
        KEY `pictogramme_group` (`id_pictogramme_group`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_lang`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogramme_lang` (
        `id_pictogramme` INT(11) NOT NULL,
        `id_lang` INT(11) NOT NULL,
        `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
        `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        PRIMARY KEY (`id_pictogramme`,`id_lang`),
        KEY (`id_pictogramme`),
        KEY (`id_lang`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'pictogramme_shop`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogramme_shop` (
        `id_pictogramme` INT(11) NOT NULL,
        `id_shop` INT(11) NOT NULL,
        PRIMARY KEY (`id_pictogramme`,`id_shop`),
        KEY (`id_pictogramme`),
        KEY (`id_shop`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;'
);

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}