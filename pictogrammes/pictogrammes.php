<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ . '/pictogrammes/classes/PictogrammeGroup.php';
require_once _PS_MODULE_DIR_ . '/pictogrammes/classes/Pictogramme.php';

class Pictogrammes extends Module
{
    public function __construct()
    {
        $this->name = 'pictogrammes';
        $this->tab = 'content_management';
        $this->version = '1.0.0';
        $this->author = 'Piment Bleu';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->trans('Pictogrammes');
        $this->description = $this->trans('Add pictograms in your products pages');
        $this->confirmUninstall = $this->trans('Are you sure you want to uninstall Pictogrammes ?');
    }
    public function getContent()
    {
        return 'Configuration';
    }
    public function install()
    {
        $this->registerHook('actionObjectPictogrammeAddBefore');
        $this->registerHook('actionObjectPictogrammeGroupAddBefore');
        $this->registerHook('actionPictogrammeGroupSave');
        $this->registerHook('actionPictogrammeGroupDelete');


        if ((file_exists($this->local_path.'sql/install.php'))
        && ($this->installTab('AdminCatalog', 'AdminPictogrammesGroups', 'Pictogrammes')))
        {
            include($this->local_path.'sql/install.php');
            include($this->local_path.'sql/insert_datas_dev.php');
        }
        else {
            return false;
        }
        return parent::install();
    }
    public function uninstall()
    {
        $this->unregisterHook('actionObjectPictogrammeAddBefore');
        $this->unregisterHook('actionObjectPictogrammeGroupAddBefore');
        $this->unregisterHook('actionPictogrammeGroupSave');
        $this->unregisterHook('actionPictogrammeGroupDelete');

        if ((file_exists($this->local_path.'sql/uninstall.php'))
        && ($this->uninstallTab('AdminPictogrammesGroups')))
        {
            include($this->local_path.'sql/uninstall.php');
        } else {
            return false;
        }
        return parent::uninstall();
    }

    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->icon = 'setting applications';
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $this->trans($name);
        }
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }

    public function uninstallTab($parent)
    {
        $id_tab = (int)Tab::getIdFromClassName($parent);
        if ($id_tab) {
            $tab = new Tab((int)$id_tab);
            try {
                $tab->delete();
            }
            catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;
    }
}