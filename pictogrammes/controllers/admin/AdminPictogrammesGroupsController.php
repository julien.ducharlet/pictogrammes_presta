<?php

/**
 * @property PictogrammeGroup $object
 */
class AdminPictogrammesGroupsController extends ModuleAdminController
{

    public $bootstrap = true;
    protected $id_pictogramme;
    protected $position_identifier = 'id_pictogramme_group';
    protected $pictogramme_name;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'pictogramme_group';
        $this->list_id = 'pictogramme_group';
        $this->identifier = 'id_pictogramme_group';
        $this->className = 'PictogrammeGroup';
        $this->lang = true;
        $this->_defaultOrderBy = 'position';

        parent::__construct();

        $this->fields_list = array(
            'id_pictogramme_group' => array(
                'title' => $this->trans('ID', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'name' => array(
                'title' => $this->trans('Name', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'filter_key' => 'b!name',
                'align' => 'left',
            ),
            'count_values' => array(
                'title' => $this->trans('Values', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'orderby' => false,
                'search' => false,
            ),
            'position' => array(
                'title' => $this->trans('Position', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'filter_key' => 'a!position',
                'position' => 'position',
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'search' => false
            ),
        );

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Would you like to delete the selected items ?', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
            ),
        );
    }

    /**
     * AdminController::renderList() override.
     *
     * @see AdminController::renderList()
     */
    public function renderList()
    {
        $this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function renderView()
    {
        if (($id = Tools::getValue('id_pictogramme_group'))) {
            $this->table = 'pictogramme';
            $this->className = 'Pictogramme';
            $this->identifier = 'id_pictogramme';
            $this->position_identifier = 'id_pictogramme';
            $this->list_id = 'pictogramme_values';
            $this->lang = true;

            $this->context->smarty->assign(array(
                'current' => self::$currentIndex . '&id_pictogramme_group=' . (int)$id . '&viewpictogramme_group',
            ));

            if (!Validate::isLoadedObject($obj = new PictogrammeGroup((int)$id))) {
                $this->errors[] = $this->trans('An error occurred while updating the status for an object.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups') .
                    ' <b>' . $this->table . '</b> ' .
                    $this->trans('(cannot load object)', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');

                return;
            }

            $this->pictogramme_name = $obj->name;
            $this->fields_list = array(
                'id_pictogramme' => array(
                    'title' => $this->trans('ID', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'align' => 'center',
                    'class' => 'fixed-width-xs',
                ),
                'pictogramme' => array(
                    'title' => $this->trans('Pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'align' => 'center',
                    'image' => 'pi',
                    'image_id' => 'id_pictogramme',
                    'orderby' => false,
                    'search' => false,
                ),
                'name' => array(
                    'title' => $this->trans('Nom', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'width' => 'auto',
                    'filter_key' => 'b!name',
                ),
            );

            $this->fields_list['position'] = array(
                'title' => $this->trans('Position', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'filter_key' => 'a!position',
                'position' => 'position',
                'class' => 'fixed-width-md',
                'search' => false,
            );

            //$this->addRowAction('view');
            $this->addRowAction('edit');
            $this->addRowAction('delete');

            $this->_where = 'AND a.`id_pictogramme_group` = ' . (int)$id;
            $this->_orderBy = 'position';

            self::$currentIndex = self::$currentIndex . '&id_pictogramme_group=' . (int)$id . '&viewpictogramme_group';
            $this->processFilter();

            return parent::renderList();
        }
    }

    /**
     * AdminController::renderForm() override.
     *
     * @see AdminController::renderForm()
     */
    public function renderForm()
    {
        $this->table = 'pictogramme_group';
        $this->identifier = 'id_pictogramme_group';

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Pictogrammes', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'icon' => 'icon-info-sign',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->trans('Name', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'name' => 'name',
                    'lang' => true,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups') . ' <>;=#{}',
                ),
            ),
        );

        // Multi-shop
        /*if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->trans('Shop association', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'name' => 'checkBoxShopAsso',
            );
        }*/

        $this->fields_form['submit'] = array(
            'title' => $this->trans('Save', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
        );

        if (!($obj = $this->loadObject(true))) {
            return;
        }

        return parent::renderForm();
    }

    public function renderFormPictogrammes()
    {
        $pictogrammes_groups = PictogrammeGroup::getPictogrammesGroups($this->context->language->id);

        $this->table = 'pictogramme';
        $this->identifier = 'id_pictogramme';

        $this->show_form_cancel_button = true;
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Values', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'icon' => 'icon-info-sign',
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->trans('Category of the pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'name' => 'id_pictogramme_group',
                    'required' => true,
                    'options' => array(
                        'query' => $pictogrammes_groups,
                        'id' => 'id_pictogramme_group',
                        'name' => 'name',
                    ),
                    'hint' => $this->trans('Choose the pictogram group for this value.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Name of the pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'name' => 'name',
                    'lang' => true,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups') . ' <>;=#{}',
                ),
            ),
        );

        // Multi-shop
        /*if (Shop::isFeatureActive()) {
            $sql = 'SELECT id_pictogramme_group, id_shop FROM ' . _DB_PREFIX_ . 'pictogramme_group_shop';
            $associations = array();
            foreach (Db::getInstance()->executeS($sql) as $row) {
                $associations[$row['id_pictogramme_group']][] = $row['id_shop'];
            }

            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->trans('Shop association', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'name' => 'checkBoxShopAsso',
                'values' => Shop::getTree(),
            );
        } else {
            $associations = array();
        }

        $this->fields_form['shop_associations'] = json_encode($associations);*/

        // Image upload
        /*$this->fields_form['input'][] = array(
            'type' => 'file',
            'label' => $this->trans('Image of the pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
            'name' => 'image',
        );*/

        // to do textarea description

        $this->fields_form['submit'] = array(
            'title' => $this->trans('Save', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
        );

        $this->fields_form['buttons'] = array(
            'save-and-stay' => array(
                'title' => $this->trans('Save then add another value', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'name' => 'submitAdd' . $this->table . 'AndStay',
                'type' => 'submit',
                'class' => 'btn btn-default pull-right',
                'icon' => 'process-icon-save',
            ),
        );

        $this->fields_value['id_pictogramme_group'] = (int)Tools::getValue('id_pictogramme_group');


        $this->table = 'pictogramme';
        $this->className = 'Pictogramme';
        $this->identifier = 'id_pictogramme';
        $this->lang = true;
        //$this->tpl_folder = 'pictogrammes/';


        if (!$obj = new Pictogramme((int)Tools::getValue($this->identifier))) {
            return;
        }

        //$image = '../img/' . $this->fieldImageSettings['dir'] . '/' . (int) $obj->id . '.jpg';

        return parent::renderForm();
    }

    /**
     * AdminController::init() override.
     *
     * @see AdminController::init()
     */
    public function init()
    {
        if (Tools::isSubmit('updatepictogramme')) {
            $this->display = 'editPictogrammes';
        } elseif (Tools::isSubmit('submitAddpictogramme')) {
            $this->display = 'editPictogrammes';
        } elseif (Tools::isSubmit('submitAddpictogramme_group')) {
            $this->display = 'add';
        }

        parent::init();
    }

    /**
     * Override processAdd to change SaveAndStay button action.
     *
     * @see classes/AdminControllerCore::processUpdate()
     */
    public function processAdd()
    {
        if ($this->table == 'pictogramme') {
            /** @var PictogrammeGroup $object */
            $object = new $this->className();
            foreach (Language::getLanguages(false) as $language) {
                if ($object->isPictogramme(
                    (int) Tools::getValue('id_pictogramme_group'),
                    Tools::getValue('name_' . $language['id_lang']),
                    $language['id_lang']
                )) {
                    $this->errors['name_' . $language['id_lang']] = $this->trans(
                        'The pictogramme value "%1$s" already exist for %2$s language',
                        array(
                            Tools::getValue('name_' . $language['id_lang']),
                            $language['name'],
                        ),
                        'Modules.Pictogrammes.Adminpictogrammesgroups'
                    );
                }
            }

            if (!empty($this->errors)) {
                return $object;
            }
        }

        $object = parent::processAdd();

        if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay') && !count($this->errors)) {
            if ($this->display == 'add') {
                $this->redirect_after = self::$currentIndex . '&' . $this->identifier . '=&conf=3&update' . $this->table . '&token=' . $this->token;
            } else {
                $this->redirect_after = self::$currentIndex . '&id_pictogramme_group=' . (int) Tools::getValue('id_pictogramme_group') . '&conf=3&update' . $this->table . '&token=' . $this->token;
            }
        }

        if (count($this->errors)) {
            $this->setTypePictogramme();
        }

        return $object;
    }

    /**
     * Override processUpdate to change SaveAndStay button action.
     *
     * @see classes/AdminControllerCore::processUpdate()
     */
    public function processUpdate()
    {
        $object = parent::processUpdate();

        if (Tools::isSubmit('submitAdd' . $this->table . 'AndStay') && !count($this->errors)) {
            if ($this->display == 'add') {
                $this->redirect_after = self::$curentIndex . '&' . $this->identifier . '=&conf=3&update' . $this->table . '&token=' . $this->token;
            } else {
                $this->redirect_after = self::$currentIndex . '&' . $this->identifier . '=&id_pictogramme_group=' . (int) Tools::getValue('id_pictogramme_group') . '&conf=3&update' . $this->table . '&token=' . $this->token;
            }
        }

        if (count($this->errors)) {
            $this->setTypePictogramme();
        }

        return $object;
    }

    /**
     * AdminController::initContent() override.
     *
     * @see AdminController::initContent()
     */
    public function initContent()
    {
        if (Combination::isFeatureActive()) {
            if ($this->display == 'edit' || $this->display == 'add') {
                if (!($this->object = $this->loadObject(true))) {
                    return;
                }
                $this->content .= $this->renderForm();
            } elseif ($this->display == 'editPictogrammes') {
                if (!$this->object = new Pictogramme((int)Tools::getValue('id_pictogramme'))) {
                    return;
                }

                $this->content .= $this->renderFormPictogrammes();
            } elseif ($this->display != 'view' && !$this->ajax) {
                $this->content .= $this->renderList();
                $this->content .= $this->renderOptions();

                if (!Tools::getValue('submitFilterpictogramme_group', 0) && !Tools::getIsset('id_pictogramme_group')) {
                    $this->processResetFilters('pictogramme_values');
                }
            } elseif ($this->display == 'view' && !$this->ajax) {
                $this->content = $this->renderView();
            }
        } else {
            $adminPerformanceUrl = $this->context->link->getAdminLink('AdminPerformance');
            $url = '<a href="' . $adminPerformanceUrl . '#featuresDetachables">' . $this->trans('Performance', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups') . '</a>';
            $this->displayWarning($this->trans('This feature has been disabled. You can activate it here: %link%.', array('%link%' => $url), 'Modules.Pictogrammes.Adminpictogrammesgroups'));
        }

        $this->context->smarty->assign(array(
            'table' => $this->table,
            'current' => self::$currentIndex,
            'token' => $this->token,
            'content' => $this->content,
        ));
    }

    public function initPageHeaderToolbar()
    {
        if (Combination::isFeatureActive()) {
            if (empty($this->display)) {
                $this->page_header_toolbar_btn['new_pictogramme_group'] = array(
                    'href' => self::$currentIndex . '&addpictogramme_group&token=' . $this->token,
                    'desc' => $this->trans('Add new categories', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'icon' => 'process-icon-new',
                );
                $this->page_header_toolbar_btn['new_value'] = array(
                    'href' => self::$currentIndex . '&updatepictogramme&id_pictogramme_group=' . (int) Tools::getValue('id_pictogramme_group') . '&token=' . $this->token,
                    'desc' => $this->trans('Add new pictograms', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'icon' => 'process-icon-new',
                );
            }
        }

        if ($this->display == 'view') {
            $this->page_header_toolbar_btn['new_value'] = array(
                'href' => self::$currentIndex . '&updatepictogramme&id_pictogramme_group=' . (int)Tools::getValue('id_pictogramme_group') . '&token=' . $this->token,
                'desc' => $this->trans('Add new pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                'icon' => 'process-icon-new',
            );
        }

        parent::initPageHeaderToolbar();
    }

    public function initToolbar()
    {
        switch ($this->display) {
            case 'add':
            case 'edit':
            case 'editPictogrammes':
                $this->toolbar_btn['save'] = array(
                    'href' => '#',
                    'desc' => $this->trans('Save', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                );

                if ($this->display == 'editPictogrammes' && !$this->id_pictogramme) {
                    $this->toolbar_btn['save-and-stay'] = array(
                        'short' => 'SaveAndStay',
                        'href' => '#',
                        'desc' => $this->trans('Save then add another value', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                        'force_desc' => true,
                    );
                }

                $this->toolbar_btn['back'] = array(
                    'href' => $this->context->link->getAdminLink('AdminPictogrammesGroups'),
                    'desc' => $this->trans('Back to list', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                );

                break;
            case 'view':
                $this->toolbar_btn['newPictogrammes'] = array(
                    'href' => $this->context->link->getAdminLink('AdminPictogrammesGroups', true, array(), array('updatepictogramme' => 1, 'id_pictogramme_group' => (int) Tools::getValue('id_pictogramme_group'))),
                    'desc' => $this->trans('Add new pictograms', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    'class' => 'toolbar-new',
                );

                $this->toolbar_btn['back'] = array(
                    'href' => $this->context->link->getAdminLink('AdminPictogrammesGroups'),
                    'desc' => $this->trans('Back to list', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                );

                break;
            default: // list
                $this->toolbar_btn['new'] = array(
                    'href' => $this->context->link->getAdminLink('AdminPictogrammesGroups', true, array(), array('add' . $this->table => 1)),
                    'desc' => $this->trans('Add new categories', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                );
                /*if ($this->can_import) {
                    $this->toolbar_btn['import'] = array(
                        'href' => $this->context->link->getAdminLink('AdminImport', true, array(), array('import_type' => 'combinations')),
                        'desc' => $this->trans('Import', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups'),
                    );
                }*/
        }
    }

    public function initToolbarTitle()
    {
        $bread_extended = $this->breadcrumbs;

        switch ($this->display) {
            case 'edit':
                $bread_extended[] = $this->trans('Edit Pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');

                break;

            case 'add':
                $bread_extended[] = $this->trans('Add New Pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');

                break;

            case 'view':
                if (Tools::getIsset('viewpictogramme_group')) {
                    if (($id = Tools::getValue('id_pictogramme_group'))) {
                        if (Validate::isLoadedObject($obj = new PictogrammeGroup((int) $id))) {
                            $bread_extended[] = $obj->name[$this->context->employee->id_lang];
                        }
                    }
                } else {
                    $bread_extended[] = $this->pictogramme_name[$this->context->employee->id_lang];
                }

                break;

            case 'editPictogrammes':
                if ($this->id_pictogramme) {
                    if (($id = Tools::getValue('id_pictogramme_group'))) {
                        if (Validate::isLoadedObject($obj = new PictogrammeGroup((int) $id))) {
                            $bread_extended[] = '<a href="' . Context::getContext()->link->getAdminLink('AdminPictogrammesGroups') . '&id_pictogramme_group=' . $id . '&viewpictogramme_group">' . $obj->name[$this->context->employee->id_lang] . '</a>';
                        }
                        if (Validate::isLoadedObject($obj = new Pictogramme((int) $this->id_pictogramme))) {
                            $bread_extended[] = $this->trans(
                                'Edit: %value%',
                                array(
                                    '%value%' => $obj->name[$this->context->employee->id_lang],
                                ),
                                'Modules.Pictogrammes.Adminpictogrammesgroups'
                            );
                        }
                    } else {
                        $bread_extended[] = $this->trans('Edit Pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                    }
                } else {
                    $bread_extended[] = $this->trans('Add New Pictogram', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                }

                break;
        }

        if (count($bread_extended) > 0) {
            $this->addMetaTitle($bread_extended[count($bread_extended) - 1]);
        }

        $this->toolbar_title = $bread_extended;
    }

    public function initProcess()
    {
        $this->setTypePictogramme();

        if (Tools::getIsset('viewpictogramme_group')) {
            $this->list_id = 'pictogramme_values';

            if (isset($_POST['submitReset' . $this->list_id])) {
                $this->processResetFilters();
            }
        } else {
            $this->list_id = 'pictogramme_group';
        }

        parent::initProcess();

        if ($this->table == 'pictogramme') {
            $this->display = 'editPictogrammes';
            $this->id_pictogramme = (int) Tools::getValue('id_pictogramme');
        }
    }

    protected function setTypePictogramme()
    {
        if (Tools::isSubmit('updatepictogramme') || Tools::isSubmit('deletepictogramme') || Tools::isSubmit('submitAddpictogramme') || Tools::isSubmit('submitBulkdeletepictogramme')) {
            $this->table = 'pictogramme';
            $this->className = 'Pictogramme';
            $this->identifier = 'id_pictogramme';

            if ($this->display == 'edit') {
                $this->display = 'editPictogrammes';
            }
        }
    }

    public function processPosition()
    {
        if (Tools::getIsset('viewpictogramme_group')) {
            $object = new Pictogramme((int) Tools::getValue('id_pictogramme'));
            self::$currentIndex = self::$currentIndex . '&viewpictogramme_group';
        } else {
            $object = new PictogrammeGroup((int) Tools::getValue('id_pictogramme_group'));
        }

        if (!Validate::isLoadedObject($object)) {
            $this->errors[] = $this->trans('An error occurred while updating the status for an object.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups') .
                ' <b>' . $this->table . '</b> ' . $this->trans('(cannot load object)', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
        } elseif (!$object->updatePosition((int) Tools::getValue('way'), (int) Tools::getValue('position'))) {
            $this->errors[] = $this->trans('Failed to update the position.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
        } else {
            $id_identifier_str = ($id_identifier = (int) Tools::getValue($this->identifier)) ? '&' . $this->identifier . '=' . $id_identifier : '';
            $redirect = self::$currentIndex . '&' . $this->table . 'Orderby=position&' . $this->table . 'Orderway=asc&conf=5' . $id_identifier_str . '&token=' . $this->token;
            $this->redirect_after = $redirect;
        }

        return $object;
    }

    /**
     * Call the right method for creating or updating object.
     *
     * @return mixed
     */
    public function processSave()
    {
        if ($this->display == 'add' || $this->display == 'edit') {
            $this->identifier = 'id_pictogramme_group';
        }

        if (!$this->id_object) {
            return $this->processAdd();
        } else {
            return $this->processUpdate();
        }
    }

    public function postProcess()
    {
        if (!Combination::isFeatureActive()) {
            return;
        }

        if (!Tools::getValue($this->identifier) && Tools::getValue('id_pictogramme') && !Tools::getValue('pictogrammeOrderby')) {
            // Override var of Controller
            $this->table = 'pictogramme';
            $this->className = 'Pictogramme';
            $this->identifier = 'id_pictogramme';
        }

        /* set location with current index */
        if (Tools::getIsset('id_pictogramme_group') && Tools::getIsset('viewpictogramme_group')) {
            self::$currentIndex = self::$currentIndex . '&id_pictogramme_group=' . Tools::getValue('id_pictogramme_group', 0) . '&viewpictogramme_group';
        }

        // If it's a pictogram, load object Pictogramme()
        if (Tools::getValue('updatepictogramme') || Tools::isSubmit('deletepictogramme') || Tools::isSubmit('submitAddpictogramme')) {
            if (true !== $this->access('edit')) {
                $this->errors[] = $this->trans('You do not have permission to edit this.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');

                return;
            } elseif (!$object = new Pictogramme((int) Tools::getValue($this->identifier))) {
                $this->errors[] = $this->trans('An error occurred while updating the status for an object.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups') .
                    ' <b>' . $this->table . '</b> ' .
                    $this->trans('(cannot load object)', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');

                return;
            }

            if (Tools::getValue('position') !== false && Tools::getValue('id_pictogramme')) {
                $_POST['id_pictogramme_group'] = $object->id_pictogramme_group;
                if (!$object->updatePosition((int) Tools::getValue('way'), (int) Tools::getValue('position'))) {
                    $this->errors[] = $this->trans('Failed to update the position.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                } else {
                    Tools::redirectAdmin(self::$currentIndex . '&conf=5&token=' . Tools::getAdminTokenLite('AdminPictogrammesGroups') . '#details_details_' . $object->id_pictogramme_group);
                }
            } elseif (Tools::isSubmit('deletepictogramme') && Tools::getValue('id_pictogramme')) {
                if (!$object->delete()) {
                    $this->errors[] = $this->trans('Failed to delete the pictogram.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                } else {
                    Tools::redirectAdmin(self::$currentIndex . '&conf=1&token=' . Tools::getAdminTokenLite('AdminPictogrammesGroups'));
                }
            } elseif (Tools::isSubmit('submitAddpictogramme')) {
                Hook::exec('actionObjectPictogrammeAddBefore');
                $this->action = 'save';
                $id_pictogramme = (int)Tools::getValue('id_pictogramme');
                // Adding last position to the pictogram if not exist
                if ($id_pictogramme <= 0) {
                    $sql = 'SELECT `position`+1
							FROM `' . _DB_PREFIX_ . 'pictogramme`
							WHERE `id_pictogramme_group` = ' . (int)Tools::getValue('id_pictogramme_group') . '
							ORDER BY position DESC';
                    // set the position of the new pictogram in $_POST for postProcess() method
                    $_POST['position'] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                }
                $_POST['id_parent'] = 0;
                $this->processSave($this->token);
            }
        } else {
            if (Tools::isSubmit('submitBulkdelete' . $this->table)) {
                if ($this->access('delete')) {
                    if (isset($_POST[$this->list_id . 'Box'])) {
                        /** @var PictogrammeGroup $object */
                        $object = new $this->className();
                        if ($object->deleteSelection($_POST[$this->list_id . 'Box'])) {
                            PictogrammeGroup::cleanPositions();
                            Tools::redirectAdmin(self::$currentIndex . '&conf=2' . '&token=' . $this->token);
                        }
                        $this->errors[] = $this->trans('An error occurred while deleting this selection.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                    } else {
                        $this->errors[] = $this->trans('You must select at least one element to delete.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                    }
                } else {
                    $this->errors[] = $this->trans('You do not have permission to delete this.', array(), 'Modules.Pictogrammes.Adminpictogrammesgroups');
                }
                // clean position after delete
                PictogrammeGroup::cleanPositions();
            } elseif (Tools::isSubmit('submitAdd' . $this->table)) {
                Hook::exec('actionObjectPictogrammeGroupAddBefore');
                $id_pictogramme_group = (int) Tools::getValue('id_pictogramme_group');
                // Adding last position to the pictogram group if not exist
                if ($id_pictogramme_group <= 0) {
                    $sql = 'SELECT `position`+1
							FROM `' . _DB_PREFIX_ . 'pictogramme_group`
							ORDER BY position DESC';
                    // set the position of the new pictogram group in $_POST for postProcess() method
                    $_POST['position'] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                }
                // clean \n\r characters
                foreach ($_POST as $key => $value) {
                    if (preg_match('/^name_/Ui', $key)) {
                        $_POST[$key] = str_replace('\n', '', str_replace('\r', '', $value));
                    }
                }
                parent::postProcess();
            } else {
                parent::postProcess();
                if (Tools::isSubmit('delete' . $this->table)) {
                    PictogrammeGroup::cleanPositions();
                }
            }
        }
    }

    /**
     * AdminController::getList() override.
     *
     * @see AdminController::getList()
     *
     * @param int $id_lang
     * @param string|null $order_by
     * @param string|null $order_way
     * @param int $start
     * @param int|null $limit
     * @param int|bool $id_lang_shop
     *
     * @throws PrestaShopException
     */
    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
    }

    /**
     * Overrides parent to delete items from sublist.
     *
     * @return mixed
     */
    public function processBulkDelete()
    {
        // If we are deleting pictogrammes instead of pictogramme_groups
        if (Tools::getIsset('pictogrammeBox')) {
            $this->className = 'Pictogramme';
            $this->table = 'pictogramme';
            $this->boxes = Tools::getValue($this->table . 'Box');
        }

        $result = parent::processBulkDelete();
        // Restore vars
        $this->className = 'PictogrammeGroup';
        $this->table = 'pictogramme_group';

        return $result;
    }

    /* Modify group pictograms position */
    public function ajaxProcessUpdateGroupsPositions()
    {
        $way = (int) Tools::getValue('way');
        $id_pictogramme_group = (int) Tools::getValue('id_pictogramme_group');
        $positions = Tools::getValue('pictogramme_group');

        $new_positions = array();
        foreach ($positions as $v) {
            if (count(explode('_', $v)) == 4) {
                $new_positions[] = $v;
            }
        }

        foreach ($new_positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2]) && (int) $pos[2] === $id_pictogramme_group) {
                if ($group_pictogramme = new PictogrammeGroup((int) $pos[2])) {
                    if (isset($position) && $group_pictogramme->updatePosition($way, $position)) {
                        echo 'ok position ' . (int) $position . ' for pictogramme group ' . (int) $pos[2] . '\r\n';
                    } else {
                        echo '{"hasError" : true, "errors" : "Can not update the ' . (int) $id_pictogramme_group . ' pictogram group to position ' . (int) $position . ' "}';
                    }
                } else {
                    echo '{"hasError" : true, "errors" : "The (' . (int) $id_pictogramme_group . ') pictogram group cannot be loaded."}';
                }

                break;
            }
        }
    }

    /* Modify pictograms position */
    public function ajaxProcessUpdatePictogrammesPositions()
    {
        $way = (int) Tools::getValue('way');
        $id_pictogramme = (int) Tools::getValue('id_pictogramme');
        $id_pictogramme_group = (int) Tools::getValue('id_pictogramme_group');
        $positions = Tools::getValue('pictogramme');

        if (is_array($positions)) {
            foreach ($positions as $position => $value) {
                $pos = explode('_', $value);

                if ((isset($pos[1], $pos[2])) && (int) $pos[2] === $id_pictogramme) {
                    if ($pictogramme = new Pictogramme((int) $pos[2])) {
                        if (isset($position) && $pictogramme->updatePosition($way, $position)) {
                            echo 'ok position ' . (int) $position . ' for pictogram ' . (int) $pos[2] . '\r\n';
                        } else {
                            echo '{"hasError" : true, "errors" : "Can not update the ' . (int) $id_pictogramme . ' pictogram to position ' . (int) $position . ' "}';
                        }
                    } else {
                        echo '{"hasError" : true, "errors" : "The (' . (int) $id_pictogramme . ') pictogram cannot be loaded"}';
                    }

                    break;
                }
            }
        }
    }
}