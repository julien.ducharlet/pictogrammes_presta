<?php

/**
 * Class PictogrammeGroup.
 */
class PictogrammeGroup extends ObjectModel
{
    /** @var string Name */
    public $name;
    /** @var int $position Position */
    public $position;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'pictogramme_group',
        'primary' => 'id_pictogramme_group',
        'multilang' => true,
        'fields' => array(
            'position' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt',
            ),

            /* Lang fields */
            'name' => array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isGenericName',
                'required' => true,
                'size' => 128,
            ),
        ),
    );

    /**
     * Adds current PictogrammeGroup as a new Object to the database.
     *
     * @param bool $autoDate Automatically set `date_upd` and `date_add` column
     * @param bool $nullValues Whether we want to use NULL values instead of empty quotes values
     *
     * @return bool Whether the PictogrammeGroup has been successfully added
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function add($autoDate = false, $nullValues = false)
    {
        if ($this->position <= 0) {
            $this->position = PictogrammeGroup::getHigherPosition() + 1;
        }

        $return = parent::add($autoDate, true);
        Hook::exec('actionPictogrammeGroupSave', array('id_pictogramme_group' => $this->id));

        return $return;
    }

    /**
     * Updates the current object in the database.
     *
     * @param bool $nullValues Whether we want to use NULL values instead of empty quotes values
     *
     * @return bool Whether the PictogrammeGroup has been succesfully updated
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function update($nullValues = false)
    {
        $return = parent::update($nullValues);
        Hook::exec('actionPictogrammeGroupSave', array('id_pictogramme_group' => $this->id));

        return $return;
    }

    /**
     * Clean dead combinations
     * A combination is considered dead when its Pictogramme ID cannot be found.
     *
     * @return bool Whether the dead combinations have been successfully deleted
     */
    /*public static function cleanDeadCombinations()
    {

    }*/

    /**
     * Deletes current PictogrammeGroup from database.
     *
     * @return bool True if delete was successful
     *
     * @throws PrestaShopException
     */
    public function delete()
    {
        if (!$this->hasMultishopEntries() || Shop::getContext() == Shop::CONTEXT_ALL) {
            /* Select children in order to find linked combinations */
            $pictogrammeIds = Db::getInstance()->executeS(
                '
				SELECT `id_pictogramme`
				FROM `' . _DB_PREFIX_ . 'pictogramme`
				WHERE `id_pictogramme_group` = ' . (int)$this->id
            );
            /* Removing pictogrammes to the found combinations */
            $toRemove = array();
            foreach ($pictogrammeIds as $pictogramme) {
                $toRemove[] = (int)$pictogramme['id_pictogramme'];
            }
            /* Delete related pictogrammes */
            if (count($toRemove)) {
                if (!Db::getInstance()->execute('
				DELETE FROM `' . _DB_PREFIX_ . 'pictogramme_lang`
				WHERE `id_pictogramme`	IN (' . implode(',', $toRemove) . ')') ||
                    !Db::getInstance()->execute('
				DELETE FROM `' . _DB_PREFIX_ . 'pictogramme_shop`
				WHERE `id_pictogramme`	IN (' . implode(',', $toRemove) . ')') ||
                    !Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'pictogramme` WHERE `id_pictogramme_group` = ' . (int)$this->id)) {
                    return false;
                }
            }
            $this->cleanPositions();
        }
        $return = parent::delete();
        if ($return) {
            Hook::exec('actionPictogrammeGroupDelete', array('id_pictogramme_group' => $this->id));
        }

        return $return;
    }

    /**
     * Get all pictogrammes for a given language / group.
     *
     * @param int $idLang Language ID
     * @param int $idPictogrammeGroup PictogrammeGroup ID
     *
     * @return array Pictogrammes
     */
    public static function getPictogrammes($idLang, $idPictogrammeGroup)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }

        return Db::getInstance()->executeS('
			SELECT *
			FROM `' . _DB_PREFIX_ . 'pictogramme` p
			' . Shop::addSqlAssociation('pictogramme', 'p') . '
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme_lang` pl
				ON (p.`id_pictogramme` = pl.`id_pictogramme` AND pl.`id_lang` = ' . (int)$idLang . ')
			WHERE p.`id_pictogramme_group` = ' . (int)$idPictogrammeGroup . '
			ORDER BY `position` ASC
		');
    }

    /**
     * Get all pictogrammes groups for a given language.
     *
     * @param int $idLang Language id
     *
     * @return array Pictogrammes groups
     */
    public static function getPictogrammesGroups($idLang)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }

        return Db::getInstance()->executeS('
			SELECT DISTINCT pgl.`name`, pg.*, pgl.*
			FROM `' . _DB_PREFIX_ . 'pictogramme_group` pg
			' . Shop::addSqlAssociation('pictogramme_group', 'pg') . '
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme_group_lang` pgl
				ON (pg.`id_pictogramme_group` = pgl.`id_pictogramme_group` AND `id_lang` = ' . (int)$idLang . ')
			ORDER BY `name` ASC
		');
    }

    /**
     * Delete several objects from database.
     *
     * @param array $selection Array with PictogrammeGroup IDs
     *
     * @return bool Deletion result
     */
    public function deleteSelection($selection)
    {
        /* Also delete Pictogrammes */
        foreach ($selection as $value) {
            $obj = new PictogrammeGroup($value);
            if (!$obj->delete()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Move a group pictogramme.
     *
     * @param bool $direction Up (1) or Down (0)
     * @param int $position
     *
     * @return bool Update result
     */
    public function updatePosition($direction, $position)
    {
        if (!$res = Db::getInstance()->executeS(
            '
			SELECT pg.`position`, pg.`id_pictogramme_group` 
			FROM `' . _DB_PREFIX_ . 'pictogramme_group` pg
			WHERE pg.`id_pictogramme_group` = ' . (int)Tools::getValue('id_pictogramme_group', 1) . '
			ORDER BY pg.`position` ASC'
        )) {
            return false;
        }

        foreach ($res as $groupPictogramme) {
            if ((int)$groupPictogramme['id_pictogramme_group'] == (int)$this->id) {
                $movedGroupPictogramme = $groupPictogramme;
            }
        }

        if (!isset($movedGroupPictogramme) || !isset($position)) {
            return false;
        }

        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return Db::getInstance()->execute(
                '
			UPDATE `' . _DB_PREFIX_ . 'pictogramme_group`
			SET `position`= `position` ' . ($direction ? '- 1' : '+ 1') . '
			WHERE `position`
			' . ($direction
                    ? '> ' . (int)$movedGroupPictogramme['position'] . ' AND `position` <= ' . (int)$position
                    : '< ' . (int)$movedGroupPictogramme['position'] . ' AND `position` >= ' . (int)$position)
            ) && Db::getInstance()->execute('
			UPDATE `' . _DB_PREFIX_ . 'pictogramme_group`
			SET `position` = ' . (int)$position . '
			WHERE `id_pictogramme_group`=' . (int)$movedGroupPictogramme['id_pictogramme_group']);
    }

    /**
     * Reorder group pictogramme position
     * Call it after deleting a group pictogramme.
     *
     * @return bool $return
     */
    public static function cleanPositions()
    {
        $return = true;
        Db::getInstance()->execute('SET @i = -1', false);
        $return = Db::getInstance()->execute(
            '
				UPDATE `' . _DB_PREFIX_ . 'pictogramme_group`
				SET `position` = @i:=@i+1
				ORDER BY `position`'
        );

        return $return;
    }

    /**
     * Get the highest PictogrammeGroup position.
     *
     * @return int $position Position
     */
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
				FROM `' . _DB_PREFIX_ . 'pictogramme_group`';
        $position = Db::getInstance()->getValue($sql);

        return (is_numeric($position)) ? $position : -1;
    }
}