<?php

/**
 * Class Pictogramme.
 */
class Pictogramme extends ObjectModel
{
    /** @var int Group id which pictogramme belongs */
    public $id_pictogramme_group;

    /** @var string Name */
    public $name;
    /** @var int $position */
    public $position;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'pictogramme',
        'primary' => 'id_pictogramme',
        'multilang' => true,
        'fields' => array(
            'id_pictogramme_group' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedId',
                'required' => true
            ),
            'position' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),

            /* Lang fields */
            'name' => array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isGenericName',
                'required' => true,
                'size' => 128
            ),
        ),
    );

    /** @var string $image_dir */
    /*protected $image_dir = _PS_COL_IMG_DIR_;*/

    /**
     * PictogrammeCore constructor.
     *
     * @param int|null $id Pictogramme ID
     * @param int|null $idLang Language ID
     * @param int|null $idShop Shop ID
     */
    public function __construct($id = null, $idLang = null, $idShop = null)
    {
        parent::__construct($id, $idLang, $idShop);
        //$this->image_dir = _PS_COL_IMG_DIR_;
    }

    /**
     * @see ObjectModel::delete()
     */
    public function delete()
    {
        if (!$this->hasMultishopEntries() || Shop::getContext() == Shop::CONTEXT_ALL) {
            /* Reinitializing position */
            $this->cleanPositions((int) $this->id_pictogramme_group);
        }
        $return = parent::delete();
        if ($return) {
            Hook::exec('actionPictogrammeDelete', array('id_pictogramme' => $this->id));
        }

        return $return;
    }

    /**
     * @see ObjectModel::update()
     */
    public function update($nullValues = false)
    {
        $return = parent::update($nullValues);

        if ($return) {
            Hook::exec('actionPictogrammeSave', array('id_pictogramme' => $this->id));
        }

        return $return;
    }

    /**
     * Adds current Pictogramme as a new Object to the database.
     *
     * @param bool $autoDate Automatically set `date_upd` and `date_add` column
     * @param bool $nullValues Whether we want to use NULL values instead of empty quotes values
     *
     * @return bool Whether the Pictogramme has been successfully added
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function add($autoDate = true, $nullValues = false)
    {
        if ($this->position <= 0) {
            $this->position = Pictogramme::getHigherPosition($this->id_pictogramme_group) + 1;
        }

        $return = parent::add($autoDate, $nullValues);

        if ($return) {
            Hook::exec('actionPictogrammeSave', array('id_pictogramme' => $this->id));
        }

        return $return;
    }

    /**
     * Get all pictogrammes for a given language.
     *
     * @param int $idLang Language ID
     * @param bool $notNull Get only not null fields if true
     *
     * @return array Pictogrammes
     */
    public static function getPictogrammes($idLang, $notNull = false)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }

        return Db::getInstance()->executeS('
			SELECT DISTINCT pg.*, pgl.*, p.`id_pictogramme`, pl.`name`, pgl.`name` AS `pictogramme_group`
			FROM `' . _DB_PREFIX_ . 'pictogramme_group` pg
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme_group_lang` pgl
				ON (pg.`id_pictogramme_group` = pgl.`id_pictogramme_group` AND pgl.`id_lang` = ' . (int) $idLang . ')
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme` p
				ON p.`id_pictogramme_group` = pg.`id_pictogramme_group`
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme_lang` pl
				ON (p.`id_pictogramme` = pl.`id_pictogramme` AND pl.`id_lang` = ' . (int) $idLang . ')
			' . Shop::addSqlAssociation('pictogramme_group', 'pg') . '
			' . Shop::addSqlAssociation('pictogramme', 'p') . '
			' . ($notNull ? 'WHERE p.`id_pictogramme` IS NOT NULL AND pl.`name` IS NOT NULL AND pgl.`id_pictogramme_group` IS NOT NULL' : '') . '
			ORDER BY pgl.`name` ASC, p.`position` ASC
		');
    }

    /**
     * Check if the given name is a Pictogramme within the given PictogrammeGroup.
     *
     * @param int $idPictogrammeGroup PictogrammeGroup
     * @param string $name Pictogramme name
     * @param int $idLang Language ID
     *
     * @return array|bool
     */
    public static function isPictogramme($idPictogrammeGroup, $name, $idLang)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }

        $result = Db::getInstance()->getValue('
			SELECT COUNT(*)
			FROM `' . _DB_PREFIX_ . 'pictogramme_group` pg
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme_group_lang` pgl
				ON (pg.`id_pictogramme_group` = pgl.`id_pictogramme_group` AND pgl.`id_lang` = ' . (int) $idLang . ')
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme` p
				ON p.`id_pictogramme_group` = pg.`id_pictogramme_group`
			LEFT JOIN `' . _DB_PREFIX_ . 'pictogramme_lang` p
				ON (p.`id_pictogramme` = pl.`id_pictogramme` AND pl.`id_lang` = ' . (int) $idLang . ')
			' . Shop::addSqlAssociation('pictogramme_group', 'pg') . '
			' . Shop::addSqlAssociation('pictogramme', 'p') . '
			WHERE pl.`name` = \'' . pSQL($name) . '\' AND pg.`id_pictogramme_group` = ' . (int) $idPictogrammeGroup . '
			ORDER BY pgl.`name` ASC, p.`position` ASC
		');

        return (int) $result > 0;
    }

    /**
     * Move an pictogramme inside its group.
     *
     * @param bool $direction Up (1) or Down (0)
     * @param int $position Current position of the pictogramme
     *
     * @return bool Update result
     */
    public function updatePosition($direction, $position)
    {
        if (!$idPictogrammeGroup = (int) Tools::getValue('id_pictogramme_group')) {
            $idPictogrammeGroup = (int) $this->id_pictogramme_group;
        }

        $sql = '
			SELECT p.`id_pictogramme`, p.`position`, p.`id_pictogramme_group`
			FROM `' . _DB_PREFIX_ . 'pictogramme` p
			WHERE p.`id_pictogramme_group` = ' . (int) $idPictogrammeGroup . '
			ORDER BY p.`position` ASC';

        if (!$res = Db::getInstance()->executeS($sql)) {
            return false;
        }

        foreach ($res as $pictogramme) {
            if ((int) $pictogramme['id_pictogramme'] == (int) $this->id) {
                $movedPictogramme = $pictogramme;
            }
        }

        if (!isset($movedPictogramme) || !isset($position)) {
            return false;
        }

        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases

        $res1 = Db::getInstance()->execute(
            '
			UPDATE `' . _DB_PREFIX_ . 'pictogramme`
			SET `position`= `position` ' . ($direction ? '- 1' : '+ 1') . '
			WHERE `position`
			' . ($direction
                ? '> ' . (int) $movedPictogramme['position'] . ' AND `position` <= ' . (int) $position
                : '< ' . (int) $movedPictogramme['position'] . ' AND `position` >= ' . (int) $position) . '
			AND `id_pictogramme_group`=' . (int) $movedPictogramme['id_pictogramme_group']
        );

        $res2 = Db::getInstance()->execute(
            '
			UPDATE `' . _DB_PREFIX_ . 'pictogramme`
			SET `position` = ' . (int) $position . '
			WHERE `id_pictogramme` = ' . (int) $movedPictogramme['id_pictogramme'] . '
			AND `id_pictogramme_group`=' . (int) $movedPictogramme['id_pictogramme_group']
        );

        return $res1 && $res2;
    }

    /**
     * Reorder the pictogramme position within the Pictogramme group.
     * Call this method after deleting a pictogramme from a group.
     *
     * @param int $idPictogrammeGroup Pictogramme group ID
     * @param bool $useLastPictogramme
     *
     * @return bool Whether the result was successfully updated
     */
    public function cleanPositions($idPictogrammeGroup, $useLastPictogramme = true)
    {
        Db::getInstance()->execute('SET @i = -1', false);
        $sql = 'UPDATE `' . _DB_PREFIX_ . 'pictogramme` SET `position` = @i:=@i+1 WHERE';

        if ($useLastPictogramme) {
            $sql .= ' `id_pictogramme` != ' . (int) $this->id . ' AND';
        }

        $sql .= ' `id_pictogramme_group` = ' . (int) $idPictogrammeGroup . ' ORDER BY `position` ASC';

        return Db::getInstance()->execute($sql);
    }

    /**
     * get highest position.
     *
     * Get the highest pictogramme position from a group pictogramme
     *
     * @param int $idPictogrammeGroup PictogrammeGroup ID
     *
     * @return int $position Position
     */
    public static function getHigherPosition($idPictogrammeGroup)
    {
        $sql = 'SELECT MAX(`position`)
				FROM `' . _DB_PREFIX_ . 'pictogramme`
				WHERE id_pictogramme_group = ' . (int) $idPictogrammeGroup;

        $position = Db::getInstance()->getValue($sql);

        return (is_numeric($position)) ? $position : -1;
    }
}